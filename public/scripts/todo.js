//Select DOM
const todoInput = document.querySelector(".todo__input");
const todoButton = document.querySelector(".todo__btn");
const todoList = document.querySelector(".todo-list");
const filterOption = document.querySelector(".filter-todo");

//Event Listeners
document.addEventListener("DOMContentLoaded", getTodos);
todoButton.addEventListener("click", addTodo);
todoList.addEventListener("click", deleteTodo);
filterOption.addEventListener("click", filterTodo);

//Functions

function addTodo(e) {
	//Prevent natural behaviour
	e.preventDefault();

	if (todoInput.value.length === 0) {
		alert("Input Can not be empty");
	} else {
		//Create todo div
		const todoDiv = document.createElement("div");
		todoDiv.classList.add("todo");
		//Create list
		const newTodo = document.createElement("input");
		newTodo.value = todoInput.value;
		newTodo.readOnly = true;
		//Save to local - do this last
		//Save to local
		saveLocalTodos(todoInput.value);
		//
		newTodo.classList.add("todo-item");
		todoDiv.appendChild(newTodo);
		todoInput.value = "";
		//Create Completed Button
		const editButton = document.createElement("button");
		editButton.innerHTML = `<i class="fas fa-edit"></i>`;
		editButton.classList.add("edit-btn");
		todoDiv.appendChild(editButton);
		//Create Completed Button
		const completedButton = document.createElement("button");
		completedButton.innerHTML = `<i class="fas fa-check"></i>`;
		completedButton.classList.add("complete-btn");
		todoDiv.appendChild(completedButton);
		//Create trash button
		const trashButton = document.createElement("button");
		trashButton.innerHTML = `<i class="fas fa-trash"></i>`;
		trashButton.classList.add("trash-btn");
		todoDiv.appendChild(trashButton);
		//attach final Todo
		todoList.appendChild(todoDiv);
	}
}

function deleteTodo(e) {
	const item = e.target;

	if (item.classList[0] === "trash-btn") {
		// e.target.parentElement.remove();
		const todo = item.parentElement;
		todo.classList.add("fall");
		//at the end
		removeLocalTodos(todo);
		todo.addEventListener("transitionend", (e) => {
			todo.remove();
		});
	}
	if (item.classList[0] === "complete-btn") {
		const todo = item.parentElement;
		todo.classList.toggle("completed");
		console.log(todo);
	}

	if (item.classList[0] === "edit-btn") {
		const todo = item.parentElement;
		let inputEdit = todo.firstChild;
		const editEl = item;
		removeLocalTodos(todo);
		if (editEl.innerHTML === `<i class="fas fa-refresh"></i>`) {
			inputEdit.readOnly = true;
			editEl.innerHTML = `<i class="fas fa-edit"></i>`;
		} else {
			editEl.innerHTML = `<i class="fas fa-refresh"></i>`;
			inputEdit.readOnly = false;
			inputEdit.focus();
		}
		saveLocalTodos(inputEdit.value);
	}
}

function filterTodo(e) {
	const todos = todoList.childNodes;
	todos.forEach(function (todo) {
		switch (e.target.value) {
			case "all":
				todo.style.display = "flex";
				break;
			case "completed":
				if (todo.classList.contains("completed")) {
					todo.style.display = "flex";
				} else {
					todo.style.display = "none";
				}
				break;
			case "uncompleted":
				if (!todo.classList.contains("completed")) {
					todo.style.display = "flex";
				} else {
					todo.style.display = "none";
				}
		}
	});
}

function saveLocalTodos(todo) {
	let todos;
	if (localStorage.getItem("todos") === null) {
		todos = [];
	} else {
		todos = JSON.parse(localStorage.getItem("todos"));
	}
	todos.push(todo);
	localStorage.setItem("todos", JSON.stringify(todos));
}
function removeLocalTodos(todo) {
	let todos;
	if (localStorage.getItem("todos") === null) {
		todos = [];
	} else {
		todos = JSON.parse(localStorage.getItem("todos"));
	}
	const todoIndex = todo.children[0].innerText;
	todos.splice(todos.indexOf(todoIndex), 1);
	localStorage.setItem("todos", JSON.stringify(todos));
}

function getTodos() {
	let todos;
	if (localStorage.getItem("todos") === null) {
		todos = [];
	} else {
		todos = JSON.parse(localStorage.getItem("todos"));
	}
	todos.forEach(function (todo) {
		//Create list

		//Create todo div
		const todoDiv = document.createElement("div");
		todoDiv.classList.add("todo");
		//Create list
		const newTodo = document.createElement("input");
		newTodo.value = todo;
		newTodo.readOnly = true;
		newTodo.classList.add("todo-item");
		todoDiv.appendChild(newTodo);
		todoInput.value = "";
		//Create Completed Button
		const editButton = document.createElement("button");
		editButton.innerHTML = `<i class="fas fa-edit"></i>`;
		editButton.classList.add("edit-btn");
		todoDiv.appendChild(editButton);
		//Create Completed Button
		const completedButton = document.createElement("button");
		completedButton.innerHTML = `<i class="fas fa-check"></i>`;
		completedButton.classList.add("complete-btn");
		todoDiv.appendChild(completedButton);
		//Create trash button
		const trashButton = document.createElement("button");
		trashButton.innerHTML = `<i class="fas fa-trash"></i>`;
		trashButton.classList.add("trash-btn");
		todoDiv.appendChild(trashButton);
		//attach final Todo
		todoList.appendChild(todoDiv);
	});
}
