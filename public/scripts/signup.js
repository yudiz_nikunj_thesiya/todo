document.querySelector(".form").addEventListener("submit", function signUp(e) {
	e.preventDefault();
	const name = document.getElementById("name");
	const email = document.getElementById("email");
	const mobile = document.getElementById("mobile");
	const gender = document.querySelector('input[name="gender"]:checked');
	const dob = document.getElementById("dob");
	const password = document.getElementById("password");

	let errEle = document.getElementById("err__msg");

	function showError(input, message) {
		const formControl = input.parentElement;
		console.log(formControl);
		formControl.className = "popup error";
		const errMsg = formControl.querySelector("#err__msg");
		console.log(errMsg);
		errMsg.innerText = message;
	}

	const getEmail = localStorage.getItem("email");
	const getMobile = localStorage.getItem("mobile");

	if (
		name.value === "" ||
		email.value === "" ||
		mobile.value === "" ||
		gender.value === "" ||
		dob.value === "" ||
		password.value === ""
	) {
		showError(errEle, "All Fields are Required");
	} else if (name.value.length < 4) {
		showError(errEle, "Name should be at least 4 characters");
	} else if (mobile.value.length !== 10) {
		showError(errEle, "Mobile Number must be at least 10 characters");
	} else if (email.value === getEmail && mobile.value === getMobile) {
		showError(errEle, "Mobile & Email are Already in Use");
	} else if (email.value === getEmail) {
		showError(errEle, "Email Already Exists");
	} else if (mobile.value === getMobile) {
		showError(errEle, "Mobile Already Exists");
	} else {
		localStorage.setItem("name", name.value);
		localStorage.setItem("email", email.value);
		localStorage.setItem("mobile", mobile.value);
		localStorage.setItem("gender", gender.value);
		localStorage.setItem("dob", dob.value);
		localStorage.setItem("password", password.value);
		alert("Registered Successfully! Now You can LogIn to your Account. 😃 ");
		window.location.href = "../pages/signin.html";
	}
});
