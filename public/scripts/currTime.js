let clock = document.getElementById("clock");

function showClock() {
	let date = new Date();
	clock.innerHTML = date.toLocaleTimeString();
}

function noDelaySetInterval(func, interval) {
	func();
	return setInterval(func, interval);
}

(function startSetInterval() {
	noDelaySetInterval(showClock, 1000);
})();
