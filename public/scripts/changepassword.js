const getUserPassword = localStorage.getItem("password");
const currPass = document.getElementById("current_password");
const newPass = document.getElementById("new_password");

document
	.querySelector(".form")
	.addEventListener("submit", function editProfile(e) {
		e.preventDefault();

		let errEle = document.getElementById("err__msg");

		function showError(input, message) {
			const formControl = input.parentElement;
			console.log(formControl);
			formControl.className = "popup error";
			const errMsg = formControl.querySelector("#err__msg");
			console.log(errMsg);
			errMsg.innerText = message;
		}

		if (currPass.value === "" || newPass.value === "") {
			showError(errEle, "All Fields are Required");
		} else if (getUserPassword !== currPass.value) {
			alert("Your current password is incorrect");
		} else {
			localStorage.setItem("password", newPass.value);
			alert("Password changed Successfully! 😃 ");
			window.location.href = "./profile.html";
		}
	});
