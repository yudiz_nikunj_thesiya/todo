document.querySelector(".form").addEventListener("submit", function signIn(e) {
	e.preventDefault();

	const emailMobile = document.getElementById("email_mobile");
	const password = document.getElementById("password");

	let errEle = document.getElementById("err__msg");

	function showError(input, message) {
		const formControl = input.parentElement;
		console.log(formControl);
		formControl.className = "popup error";
		const errMsg = formControl.querySelector("#err__msg");
		console.log(errMsg);
		errMsg.innerText = message;
	}

	const getEmail = localStorage.getItem("email");
	const getMobile = localStorage.getItem("mobile");
	const getPassword = localStorage.getItem("password");

	if (emailMobile.value === "" || password.value === "") {
		showError(errEle, "All Fields Are Required.");
	} else if (
		(emailMobile.value === getEmail || emailMobile.value === getMobile) &&
		password.value === getPassword
	) {
		window.location.href = "../index.html";
	} else {
		showError(errEle, "Incorrect Credentials");
	}
});
