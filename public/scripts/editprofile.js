const getName = localStorage.getItem("name");
const getEmail = localStorage.getItem("email");
const getMobile = localStorage.getItem("mobile");
const getPassword = localStorage.getItem("password");
const getDob = localStorage.getItem("dob");
const getGender = localStorage.getItem("gender");

let setName = document.getElementById("name");
let setEmail = document.getElementById("email");
let setMobile = document.getElementById("mobile");
let setGender = document.querySelector('input[name="gender"]:checked');
let setDob = document.getElementById("dob");
let setPassword = document.getElementById("password");

let male = document.getElementById("male");
let female = document.getElementById("female");

setName.value = getName;
setEmail.value = getEmail;
setMobile.value = getMobile;
setPassword.value = getPassword;
setDob.value = getDob;

if (getGender === "male") {
	male.setAttribute("checked", "true");
} else {
	female.setAttribute("checked", "false");
}

document
	.querySelector(".form")
	.addEventListener("submit", function editProfile(e) {
		e.preventDefault();

		const name = document.getElementById("name");
		const email = document.getElementById("email");
		const mobile = document.getElementById("mobile");
		const gender = document.querySelector('input[name="gender"]:checked');
		const dob = document.getElementById("dob");
		const password = document.getElementById("password");

		let errEle = document.getElementById("err__msg");

		function showError(input, message) {
			const formControl = input.parentElement;
			console.log(formControl);
			formControl.className = "popup error";
			const errMsg = formControl.querySelector("#err__msg");
			console.log(errMsg);
			errMsg.innerText = message;
		}

		if (
			name.value === "" ||
			email.value === "" ||
			mobile.value === "" ||
			gender.value === "" ||
			dob.value === "" ||
			password.value === ""
		) {
			showError(errEle, "All Fields are Required");
		} else if (name.value.length < 4) {
			showError(errEle, "Name should be at least 4 characters");
		} else if (mobile.value.length !== 10) {
			showError(errEle, "Mobile Number must be at least 10 characters");
		} else {
			localStorage.setItem("name", name.value);
			localStorage.setItem("email", email.value);
			localStorage.setItem("mobile", mobile.value);
			localStorage.setItem("gender", gender.value);
			localStorage.setItem("dob", dob.value);
			localStorage.setItem("password", password.value);
			alert("Profile Updated Successfully! 😃 ");
			window.location.href = "./profile.html";
		}
	});
