const giphy = {
	baseURL: "https://api.giphy.com/v1/gifs/",
	apiKey: "aHoLGXdfHbDdhhEWwQ1a1g1dF2cxqcJY",
	tag: "fail",
	type: "random",
	rating: "pg-13",
};

let giphyURL = encodeURI(
	giphy.baseURL +
		giphy.type +
		"?api_key=" +
		giphy.apiKey +
		"&tag=" +
		giphy.tag +
		"&rating=" +
		giphy.rating
);

function showGif() {
	fetch(giphyURL)
		.then((response) => response.json())
		.then((context) => {
			let gifEle = document.getElementById("gif__container");
			let gif = context.data.images.downsized.url;
			gifEle.style.backgroundImage = `url(${gif})`;
		})
		.catch((err) => {
			console.error(err);
		});
}

function noDelaySetInterval(func, interval) {
	func();
	return setInterval(func, interval);
}

(function startSetInterval() {
	noDelaySetInterval(showGif, 120000);
})();
